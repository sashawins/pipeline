# Python App

    $ git clone https://gitlab.com/sashawins/pipeline.git
    $ cd pipeline

    $ docker build -t python-flask .
    $ docker run -d -p 5000:5000 --name calculator python-flask

## Usage

### Interacting with API:

    curl -X POST -H "Content-Type: application/json" -d '{"a": 7, "b": 3}' http://localhost:5000/plus

`"result": 10`

    curl -X POST -H "Content-Type: application/json" -d '{"a": 7, "b": 3}' http://localhost:5000/minus

`"result": 4`

    curl -X POST -H "Content-Type: application/json" -d '{"a": 7, "b": 3}' http://localhost:5000/multiply

`"result": 21`

    curl -X POST -H "Content-Type: application/json" -d '{"a": 7, "b": 3}' http://localhost:5000/divide

`"result": 2.3333333333333335`

# CI/CD

## Stages:

- Pre-commit
- Pre-build
- build
- deploy
- test

![pipeline-stages](https://i.imgur.com/OXYriDG.png)

## Vulnerabilities

To check our source code for known vulnerabilities we can use Static Application Security Testing (SAST) if we’re using GitLab CI/CD.  
We can run SAST analyzers in any GitLab tier. The analyzers output JSON-formatted reports as job artifacts.  
![semgrep-sast](https://i.imgur.com/4kgflec.png)
