FROM python:3.8-slim

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./app.py /usr/src/app
RUN pip install flask

EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["app.py"]